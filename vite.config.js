/**
 Copyright (c) 2023, LXMS (lxinet.cn).

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
import { fileURLToPath, URL } from 'node:url'
import vue from '@vitejs/plugin-vue'
import { defineConfig } from "vite";
// import path from "path";



export default defineConfig({
  server: {
    port: 8400
    // proxy: {
    //   // 选项写法
    //   '/system': {
    //     target: 'http://127.0.0.1:8200/', // 所要代理的目标地址
    //     rewrite: path => path.replace(/^\/system/, ''), // 重写传过来的path路径，比如 `/api/index/1?id=10&name=zs`（注意:path路径最前面有斜杠（/），因此，正则匹配的时候不要忘了是斜杠（/）开头的；选项的 key 也是斜杠（/）开头的）
    //     changeOrigin: true,  // true/false, Default: false - changes the origin of the host header to the target URL
    //   }
    // }
  },

  css: {
    // css预处理器
    preprocessorOptions: {
      less: {
        modifyVars:{
          hack:'true;@import "./src/assets/styles/main.less"'
        },
        javascriptEnabled:true
      },
    },

  },
  plugins: [
    // 引入插件
    vue(),


  ],

  configureWebpack: {
    // plugins: [createThemeColorReplacerPlugin()],
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '@components':fileURLToPath(new URL('./src/components', import.meta.url)),
      '@img': fileURLToPath(new URL('./src/assets/images', import.meta.url)),

    }
  },



})




