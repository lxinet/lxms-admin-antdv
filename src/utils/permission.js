/**
 Copyright (c) 2023, LXMS (lxinet.cn).

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
export const hasPermission = {
  install(Vue, options) {
    Vue.directive('has', {
      mounted(el, binding) {
        if (!checkPermission(binding.value)) {
          el.parentNode.removeChild(el);
        }
      },
    });

    //权限检查方法
    function checkPermission(value) {
      if (value === undefined || value === "") {
        return true;
      }
      let isExist = false;
      let buttonArr = JSON.parse(localStorage.getItem("permissions") || "[]");
      if (buttonArr === ["*"]) {
        console.log("超管")
        return true;
      }
      //判断按钮是否有权限
      if (buttonArr.includes(value)) {
        isExist = true;
      }
      return isExist;

    };
  }
};
export default hasPermission;