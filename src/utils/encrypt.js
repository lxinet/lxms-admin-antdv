/**
 Copyright (c) 2023, LXMS (lxinet.cn).

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
import JsEncrypt from 'jsencrypt'
export const encrypt = {

    rasEncrypt:(text, publicKey) => {
        console.log(text, publicKey)
        let jse = new JsEncrypt()
        //公钥
        let publicString = '-----BEGIN PUBLIC KEY-----\n' + publicKey + '-----END PUBLIC KEY-----'
        jse.setPublicKey(publicString)
        // 加密内容
        return jse.encrypt(text);
    },
};


