/**
 Copyright (c) 2023, LXMS (lxinet.cn).

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
export const localStorage = {

  /**
   * Set storage
   *
   * @param name
   * @param content
   * @param maxAge
   */
  set: (name, content, maxAge = null) => {
    if (typeof content !== 'string') {
      content = JSON.stringify(content)
    }
    let storage = window.localStorage;
    storage.setItem(name, content)
    if (maxAge && !isNaN(parseInt(maxAge))) {
      let timeout = parseInt(new Date().getTime() / 1000);
      storage.setItem(`${name}_expire`, timeout + maxAge)
    }
  },
  /**
   * Get storage
   *
   * @param name
   * @returns {*}
   */
  get: name => {
    let content = window.localStorage.getItem(name);
    let _expire = window.localStorage.getItem(`${name}_expire`);
    if (_expire) {
      let now = parseInt(new Date().getTime() / 1000);
      if (now > _expire) {
        window.localStorage.removeItem(name);
        window.localStorage.removeItem(`${name}_expire`);
        return;
      }
    }

    try {
      return JSON.parse(content)
    } catch (e) {
      return content
    }
  },
  /**
   * remove storage
   *
   * @param name
   */
  remove: name => {
    window.localStorage.removeItem(name);
    window.localStorage.removeItem(`${name}_expire`)
  },
  /**
   * Clear all storage
   */
  removeAll: (list) => {
    let localList = {}
    for (const iterator of list) {
      localList[iterator] = window.localStorage[iterator]
    }
    window.localStorage.clear()
    if (list) {
      let storage = window.localStorage;
      for (const key in localList) {
        storage.setItem(key, localList[key])
      }
    }

  }
};
export const sessionStorage = {

  /**
   * Set storage
   *
   * @param name
   * @param content
   * @param maxAge
   */
  set: (name, content) => {
    if (typeof content !== 'string') {
      content = JSON.stringify(content)
    }
    let storage = window.sessionStorage;
    storage.setItem(name, content)
  },
  /**
   * Get storage
   *
   * @param name
   * @returns {*}
   */
  get: name => {
    let content = window.sessionStorage.getItem(name)
    try {
      return JSON.parse(content)
    } catch (e) {
      return content
    }
  },
  /**
   * Clear storage
   *
   * @param name
   */
  remove: name => {

    window.sessionStorage.removeItem(name);
  },
  /**
   * Clear all storage
   */
  removeAll: () => {
    window.sessionStorage.clear()
  }
};

