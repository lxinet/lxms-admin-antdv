/**
 Copyright (c) 2023, LXMS (lxinet.cn).

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
import {createRouter, createWebHashHistory, createWebHistory} from "vue-router"

const routes = [
  {
    path: "/login",
    name: "Login",
    component: () => import("@/pages/login/index.vue"),
  },
  {
    path: "/",
    name: "home",
    component: () => import('@components/baseComponents/layouts/index.vue'),
    redirect: "/index",
    meta: {title: "首页"},
    children: [
      {
        path: "/index",
        name: "/index",
        component: () => import("@/pages/index/index.vue"),
        meta: {title: "首页"},
      },
      {
        path: "/system/post",
        name: "post",
        component: () => import("@/pages/system/post/index.vue"),
        meta: {title: "岗位管理"},
      },
      {
        path: "/system/role",
        name: "role",
        component: () => import("@/pages/system/role/index.vue"),
        meta: {title: "角色管理"},
      },
      {
        path: "/system/dept",
        name: "dept",
        component: () => import("@/pages/system/dept/index.vue"),
        meta: {title: "部门管理"},
      },
      {
        path: "/system/user",
        name: "user",
        component: () => import("@/pages/system/user/index.vue"),
        meta: {title: "用户管理"},
      },
      {
        path: "/system/dict",
        name: "dict",
        component: () => import("@/pages/system/dict/index.vue"),
        meta: {title: "字典管理"},
      },
      {
        path: "/log/loginLog",
        name: "loginLog",
        component: () => import("@/pages/log/loginLog/index.vue"),
        meta: {title: "登录日志"},
      },

    ]
  }
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router