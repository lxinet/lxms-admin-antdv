/**
 Copyright (c) 2023, LXMS (lxinet.cn).

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
import { createApp } from 'vue';
import Antd from 'ant-design-vue';
import App from './App.vue';
import 'ant-design-vue/dist/reset.css';
import * as Icons from '@ant-design/icons-vue';
import '@/assets/styles/main.less';
import router from '@/router/index.js'
import {request} from '@/utils/request.js'
import { localStorage, sessionStorage } from "@/utils/storage"
import { encrypt } from "@/utils/encrypt.js"
import { message } from 'ant-design-vue';
import JsEncrypt from 'jsencrypt'
import hasPermission from '@/utils/permission.js';

import mitt from 'mitt'

const app = createApp(App);

// 循环使用全部全部图标
const icons = Icons
for (const i in icons) {
  // 全局注册一下组件
  app.component(i, icons[i])
}
app.config.warnHandler=()=> null;
app.config.globalProperties.$message = message;
app.config.globalProperties.$request = request;
app.config.globalProperties.$localStorage = localStorage;
app.config.globalProperties.$sessionStorage = sessionStorage;
app.config.globalProperties.$jsEncrypt = JsEncrypt;
app.config.globalProperties.$encrypt = encrypt;
app.config.globalProperties.$bus = mitt();

app.use(hasPermission)
app.use(router);
app.use(Antd);
app.mount('#app');