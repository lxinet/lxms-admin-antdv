/**
 Copyright (c) 2023, LXMS (lxinet.cn).

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
export const editPageMinxin = {
  data() {
    return {
      title: "",
      isShow: false,
      action: "",
      confirmLoading: false,
      filters: {},
      formData: {},
      loading: false,
      loadingDelayTime: 500
    };
  },
  methods: {
    handleOk() {
      this.$refs.lxForm.submitForm();
    },

    submitImpl() {
      this.loading = true;
      this.$request({
        url: this.url[this.action],
        method: "POST",
        data: {
          ...this.formData,
          ...this.filters
        },
      }).then((res) => {
        this.$message.success(res.msg);
        this.$parent.$refs["listTable"].loadData(1);
        this.handleCancel();
      }).finally(() => {
        this.loading = false;
      })
    },

    handleCancel() {
      this.isShow = false;
      this.formData = {};
      this.$refs.lxForm.$refs.form.resetFields();

    }
  },
  mounted() {
  }
}