/**
 Copyright (c) 2023, LXMS (lxinet.cn).

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
export const indexPageMinxin = {
  data() {
    return {
      searchConfig: [],
      queryParams: {},
      dataSource: [],
      columns: [],
      ltTools: [],
      rtTools: [],
      selectedRowKeys: [],
      rowKey: "id",
      selectedRows: [],
      loading: false,
      loadingDelayTime: 500
    };
  },
  methods: {
    handleSearch(refName = 'listTable') {
      this.selectedRowKeys = [];
      this.selectedRows = [];
      this.$refs[refName].loadData(1);
    },

    handleToolsClick(item) {
      console.log("======", item)
      switch (item.key) {
        case 'add':
          this.handleAction("editNode", item.key);
          break;
        case 'detail':
          this.handleAction("detailNode", item.key);
          break;
        case "delete":
          this.batchDelete();
          break;
        default:
          break;
      }
    },

    handleAdd(refName, key, record) {
      this.$refs[refName].title = this.$route.meta.title + "- 新增";
      this.$refs[refName].isShow = true;
      this.$refs[refName].action = "add";
    },
    handleAction(refName, key, record) {
      console.log("handleAction", refName, key)
      switch (key) {
        case "add":
          this.handleAdd(refName, key, record);
          break;
        case "edit":
          this.$refs[refName].title = this.$route.meta.title + "- 修改";
          this.$refs[refName].isShow = true;
          this.$refs[refName].formData = record;
          this.$refs[refName].action = "edit";
          break;
        case "detail":
          this.$refs[refName].title = this.$route.meta.title + "- 详情";
          this.$refs[refName].isShow = true;
          this.$refs[refName].item = record;
          this.$refs[refName].action = "detail";
          break;
        default:
          this.$refs[refName].isShow = true;
          break;
      }

    },

    batchDelete(params = {}) {
      if (!this.url.batchDelete) {
        this.$message.error("请设置url.batchDelete属性!");
        return;
      }
      if (this.selectedRowKeys.length <= 0) {
        this.$message.warning("请选择一条记录！");
        return;
      } else {
        this.$confirm({
          title: "确认删除",
          content: "是否删除选中数据?",
          onOk: () => {
            this.batchDeleteImpl(params);

          }

        })
      }
    },

    batchDeleteImpl(params) {
      this.loading = true;
      this.$request({
        url: this.url.batchDelete,
        method: "post",
        data: {ids: this.selectedRowKeys, ...params},
      }).then((res) => {
        this.$message.success(res.msg);
        this.$refs["listTable"].loadData();
        this.selectedRowKeys = [];
      }).finally(() => {
        this.loading = false;
      });
    },
    handleDelete(record, params = {}) {
      if (!this.url.delete) {
        this.$message.error("请设置url.delete属性!");
        return;
      }
      this.loading = true;
      this.$request({
        method: "post",
        url: `${this.url.delete}/${record[this.rowKey]}`
      }).then((res) => {
        this.$refs["listTable"].loadData();
        this.$message.success(res.msg);
      }).finally(() => {
        this.loading = false;
      });
    },


  },
  mounted() {
  }
}