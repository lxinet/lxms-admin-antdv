# 凌夕管理系统（LXMS）前端工程

## lxms-admin-antdv

### 简介

凌夕管理系统（LXMS）前端工程，基于 Ant Design Vue 前端框架开发。

服务端仓库地址：[lxms](https://gitee.com/lxinet/lxms)


### 开发

```sh
# 进入项目目录
cd lxms-admin-antdv

# 安装依赖
npm install

# 可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npmmirror.com

# 启动服务
npm run dev
```
浏览器访问 http://localhost:8400

### 发布

```sh
npm run build
```
